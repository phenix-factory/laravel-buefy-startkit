<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdatePassword;
use App\Http\Requests\UpdateUser;
use App\Models\User;
use Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function show(Request $request)
    {
        return $request->user();
    }

    public function update(UpdateUser $request)
    {
        $user = User::find(auth()->id());
        $user->update($request->all());
        $user->refresh();
        return $user;
    }

    public function password(UpdatePassword $request)
    {
        $user = User::find(auth()->id());
        $user->update([
            'password' => Hash::make($request->input('newPassword'))
        ]);
        $user->refresh();
        return $user;
    }
}
