/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require("axios");

window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
window.axios.defaults.headers.common["Accept"] = "application/json";
window.axios.defaults.withCredentials = true;

import Vue from "vue";
import VueRouter from "vue-router";
import Buefy from "buefy";
import { routes } from "./routes.js";
import { store } from "./store.js";
import { DialogProgrammatic as Dialog } from "buefy";

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

// const token = document.head.querySelector('meta[name="csrf-token"]');

// if (token) {
//     window.axios.defaults.headers.common["X-CSRF-TOKEN"] = token.content;
// } else {
//     console.error(
//         "CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token"
//     );
// }


Vue.use(VueRouter);
Vue.use(Buefy);

Vue.component(
    "header-container",
    require("./components/layout/header-container.vue").default
);
Vue.component(
    "main-container",
    require("./components/layout/main-container.vue").default
);

const router = new VueRouter({
    mode: 'history',
    routes,
});

axios.interceptors.response.use(
    function (response) {
        // Any status code that lie within the range of 2xx cause this function to trigger
        // Do something with response data
        return response;
    },
    function (error) {
        // Any status codes that falls outside the range of 2xx cause this function to trigger
        // Do something with response error


        if (error.response.status === 401) {
            store.commit('user/deleteUser');
            router.push({
                name: 'login'
            });
        }

        if (error.response.status === 403) {
            Dialog.alert({

              title: "Error: Unautorized",
                message: "This action is unautorized.",
                type: "is-danger",
                hasIcon: true,
                icon: "alert-circle",
                ariaRole: "alertdialog",
                ariaModal: true,
            });
        }

        if (error.response.status === 422) {
            store.commit('form/setMessage', error.response.data.message);
            store.commit('form/setErrors', error.response.data.errors);
        }

        if (error.response.status === 500) {
            Dialog.alert({
                title: "Server error",
                message: "This action caused a server error.",
                type: "is-danger",
                hasIcon: true,
                icon: "alert-circle",
                ariaRole: "alertdialog",
                ariaModal: true,
            });
        }

        return Promise.reject(error);
    }
);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: "#app",
    router,
    store
});
