export default {
    methods: {
        computeDate(dateValue) {
            if (dateValue) {
                const options = {
                    hour12: false,
                    day: 'numeric',
                    month: 'long',
                    year: 'numeric',
                    hour: '2-digit',
                    minute: '2-digit',
                };
                const date = new Date(dateValue);
                return new Intl.DateTimeFormat("fr", options).format(date);
            }
            return undefined;
        }
    }
};
