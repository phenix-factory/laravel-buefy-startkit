export default {
    created() {
        if (this.title) {
            document.title = this.title;
        } else {
            console.error('Missing title');
        }
    },
    watch: {
        title: function (value) {
            if (value) {
                document.title = value;
            }
        }
    }
};
