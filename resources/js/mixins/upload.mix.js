export default {
    methods: {
        uploadImage(file, model, model_id, type) {
            var data = new FormData();
            data.append('model', model);
            data.append('model_id', model_id);
            data.append('link_type', type);
            data.append('file', file);

            return axios.post('/api/image', data, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });
        },
        uploadImages(files, model, model_id, type) {
            // Build a array of promise
            files = files.map((file) => {
                return this.uploadImage(file, model, model_id, type);
            });
            return axios.all(files);
        },
        updateImage(image_id, file) {
            var data = new FormData();
            data.append('file', file);

            return axios.post('/api/image/' + image_id, data, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });
        },
    }
};
