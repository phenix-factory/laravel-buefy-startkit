import { store } from './store';
import Home from './components/pages/home';
import Login from './components/pages/login';
import Register from "./components/pages/register";
import Logout from "./components/pages/logout";
import Profil from './components/pages/profil';

/**
 * This guard will reconnect user if they have a API session.
 * Redirect to login if they don't have a localstorage or a API session
 */
async function authGuard(next, roles = []) {
    if (store.getters['user/isConnected']) {
        if (!roles.length) {
            next();
        } else {
            const hasRole = store.getters['user/hasRole'];
            if (hasRole(roles)) {
                next();
            }
            // TODO: should redirect to a 403
        }
    } else {
        // Guard will try to get the current user from the API
        var user = await axios.get('api/user/').catch((error) => {
            next({ name: 'login' });
        });
        if (user.data) {
            store.commit('user/setUser', user.data);
            next();
        }
    }
}

export const routes = [
    { name: 'home', path: "/", component: Home, props: (route) => ({ page: Number(route.query.page) })},
    { name: 'login', path: "/login", component: Login },
    { name: 'register', path: "/register", component: Register },
    { name: 'logout', path: "/logout", component: Logout },
    {
        name: "profil",
        path: "/user/profil",
        component: Profil,
        beforeEnter: (to, from, next) => authGuard(next)
    }
];
