import Vue from "vue";
import Vuex from "vuex";
import { userStore } from "./store/user.store";
import { formStore } from "./store/form.store";
Vue.use(Vuex);

export const store = new Vuex.Store({
    stric: process.env.MIX_VUE_DEBUG,
    modules: {
        user: userStore,
        form: formStore
    }
});
