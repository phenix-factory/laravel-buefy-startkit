const defaultState = () => ({
    success: false,
    message: undefined,
    errors: {}
});

export const formStore = {
    namespaced: true,
    state: defaultState,
    getters: {
        success(state) {
            return state.success;
        },
        error(state) {
            return (field) => state.errors[field];
        }
    },
    mutations: {
        setSuccess(state, success) {
            state.success = success;
        },
        setMessage(state, message) {
            state.message = message;
        },
        setErrors(state, errors) {
            state.errors = errors;
        }
    }
};
