import storage from "../localstorage.js";

const defaultState = () => ({
    user: storage.getItem("user")
});

export const userStore = {
    namespaced: true,
    state: defaultState,
    getters: {
        isConnected(state) {
            if (state.user !== null && state.user.id) {
                return true;
            } else {
                return false;
            }
        },
        user(state) {
            return state.user;
        }
    },
    mutations: {
        setUser(state, user) {
            state.user = user;
            storage.setItem("user", user);
        },
        deleteUser(state) {
            state.user = {};
            storage.removeItem("user");
        }
    }
};
